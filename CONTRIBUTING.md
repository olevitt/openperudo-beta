# Contribuer à OpenPerudo

OpenPerudo n'avance qu'à partir de contributions.  
Afin que tout le monde puisse contribuer sereinement, sans peur de tout casser ou de faire une erreur, ce projet va utiliser pleinement la puissance des branches ainsi que des [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html).

## Déclarer un bug, proposer une fonctionnalité, lancer une discussion

Pour déclarer un bug, proposer une fonctionnalité ou lancer une discussion sur le projet, il vous faut créer une issue sur le projet : https://gitlab.com/openperudo/openperudo-ui/-/issues  
Les issues sont des espaces de discussion ouverts, **tout le monde peut créer une issue ou discuter dans une issue**.

## Ajouter du code

Le projet a une unique branche : `master`. L'écriture directe (push) dans cette branche est désactivée (branche protégée). Pour ajouter du code au dépôt il faut donc passer par des merge requests qui sont des propositions de code. Propositions qui sont ensuite débattues par la communauté, validées (ou non) puis fusionnées dans la branche `master` du dépôt.

## Soumettre une merge request

Les étapes pour soumettre une merge request sont les suivantes :

1. Forker le dépôt
2. Créer une branche dans votre fork
3. Pusher votre code dans cette branche
4. Créer la merge request

### 1. Forker le dépôt

Le dépôt principal est vérouillé en écriture. En plus de cela, vous n'êtes pas membre du projet. Vos contributions doivent donc d'abord être publiées sur votre propre version du dépôt. On parle de fork.  
La procédure est décrite (par exemple) ici : https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

Vous avez maintenant votre propre version du dépôt OpenPerudo !  
A noter : cette étape n'est à faire qu'une seule fois. Par la suite, il vous suffira de mettre à jour votre fork pour suivre les changements du dépôt principal. Vous pouvez utiliser le [mirroring de gitlab](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/) ou la ligne de commande git : https://forum.gitlab.com/t/refreshing-a-fork/32469/2

### 2. Créer une branche dans votre fork

Il est fortement conseillé de ne pas développer vos fonctionnalités dans la branche `master` (car l'histoire de `master` est amenée à évoluer pendant que vous travaillez).  
Créez donc une branche dans votre fork :

```
git checkout -b ma-fonctionnalite
```

(choisissez un nom de branche en lien avec la fonctionnalité que vous développez)

### 3. Pusher votre code dans cette branche

Vous pouvez maintenant travailler dans votre branche et y pusher tous vos changements sans risque :)

### 4. Créer la merge request

Quand vous le souhaitez, vous pouvez créer la demande de fusion (merge request) afin de proposer le code de votre branche.  
Le processus est décrit ici : https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html  
A noter : vous pouvez tout à fait créer une merge request alors que votre code n'est pas terminé. Si vous souhaitez préciser que le travail est toujours en cours et que le code ne doit donc pas être fusionné tout de suite, vous pouvez utiliser préfixer le titre par `WIP:` pour signifier qu'il s'agit d'un `Work In Progress`

## Débat et validations des merge requests

Seuls les maintainers (responsables du projet) peuvent valider une merge request.  
La liste des maintainers est volontairement réduite au strict minimum.  
Une merge request doit donc d'abord être relue (code review), débattue et validée avant d'être mergée par l'un des maintainers.  
Lorsqu'une merge request est mergée, tout le code qu'elle contient est automatiquement fusionné dans la branche de destination (ici `master`).  
L'historique de `master` est donc uniquement composé de merge requests acceptées ce qui le rend très clair et très lisible.

## Ajout de nouveaux maintainers

Les étudiants souhaitant devenir maintainers sont invités à se faire connaitre.  
**Maintainer ne veut pas dire "la personne qui écrit le code"**. Le rôle du maintainer est d'accepter (ou refuser) les merge requests et d'animer la communauté (éventuellement trancher les débats et trier / fermer les issues qui ne sont plus pertinentes).  
Il peut évidemment contribuer au code au même titre que n'importe qui mais c'est une condition ni nécessaire ni suffisante.
